<?php
	require(__DIR__."/form-day.php");
?>

<div class="page-wrapper">
 	
	<!-- Events Section -->
	<section class="events-section">
		<div class="auto-container">
			<!-- Events Info Tabs -->
            <div class="events-info-tabs">
                <!-- Events Tabs -->
                <div class="events-tabs tabs-box">
                
                    <!--Tab Btns-->
                    <ul class="tab-btns tab-buttons clearfix">
                        <li data-tab="#day-one" class="tab-btn active-btn">Day 1 <span>Sunday - June 17, 2020</span></li>
                        <li data-tab="#day-two" class="tab-btn">Day 2 <span>Monday - June 18, 2020</span></li>
                        <li data-tab="#day-three" class="tab-btn">Day 3 <span>Tuesday - June 19, 2020</span></li>
                    </ul>
<!-- 
					<div class="buttons-box text-left">
						<a href="#add-new-day-form" class="ts-image-popup theme-btn btn-style-one">Add New Day</a>
					</div> -->
                    
                    <!--Tabs Container-->
                    <div class="tabs-content">

                        <!--Tab / Active Tab-->
                        <div class="tab active-tab" id="day-one">
                            <div class="content">

								<!-- Event block -->
								<div class="event-block">
									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-1">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-1.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">9:00 - 10:00</div>
													<h2>The Modern Engineering Methods</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Jeff Robins</a>
														<span>Partner Startup</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-1">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">
														<!-- Change Thumbnail from Session -->
														<img style="margin: 35px;" src="<?php echo __ROOT_URL;?>/assets/images/icons/cronometer.png" alt="" width="100" height="auto" />

													</div>
												</div>
											</div>
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">10:00 - 10:30</div>
													<h2>Coffee Break</h2>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-2">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-2.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">10:30 - 12:00</div>
													<h2>Focus On Dynamics</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Robert Gates</a>
														<span>Partner Startup</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-2">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="inner-box">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">
														<!-- Change Thumbnail from Session -->
														<img style="margin: 35px;" src="<?php echo __ROOT_URL;?>/assets/images/icons/cronometer.png" alt="" width="100" height="auto" />

													</div>
												</div>
											</div>
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">12:00 - 13:30</div>
													<h2>Lunch Time!</h2>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-3">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-3.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">13:30 - 14:30</div>
													<h2>Evolution of User Experience</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Danielle Ahn</a>
														<span>Co-founder ZipCar, </span>
														<a href="#popup_1" class="ts-image-popup">Piera Mcclure</a>
														<span>Creative Director Slack</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-3">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-4">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-4.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">14:30 - 15:30</div>
													<h2>Building Gov as a Service</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Piera Mcclure</a>
														<span>Creative Director Slack</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-4">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						
						<!-- Tab -->
                        <div class="tab" id="day-two">
							<div class="content">

								<!-- Event block -->
								<div class="event-block">
									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-5">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-4.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">9:00 - 10:00</div>
													<h2>The Modern Engineering Methods</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Jeff Robins</a>
														<span>Partner Startup</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-5">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">
														<!-- Change Thumbnail from Session -->
														<img style="margin: 35px;" src="<?php echo __ROOT_URL;?>/assets/images/icons/cronometer.png" alt="" width="100" height="auto" />

													</div>
												</div>
											</div>
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">10:00 - 10:30</div>
													<h2>Coffee Break</h2>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-6">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-1.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">10:30 - 12:00</div>
													<h2>Focus On Dynamics</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Robert Gates</a>
														<span>Partner Startup</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-6">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="inner-box">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">
														<!-- Change Thumbnail from Session -->
														<img style="margin: 35px;" src="<?php echo __ROOT_URL;?>/assets/images/icons/cronometer.png" alt="" width="100" height="auto" />

													</div>
												</div>
											</div>
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">12:00 - 13:30</div>
													<h2>Lunch Time!</h2>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-7">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-2.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">13:30 - 14:30</div>
													<h2>Evolution of User Experience</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Danielle Ahn</a>
														<span>Co-founder ZipCar, </span>
														<a href="#popup_1" class="ts-image-popup">Piera Mcclure</a>
														<span>Creative Director Slack</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-7">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-8">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-4.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">14:30 - 15:30</div>
													<h2>Building Gov as a Service</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Piera Mcclure</a>
														<span>Creative Director Slack</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-8">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						
						<!-- Tab -->
                        <div class="tab" id="day-three">
							<div class="content">

								<!-- Event block -->
								<div class="event-block">
									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-9">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-3.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">9:00 - 10:00</div>
													<h2>The Modern Engineering Methods</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Jeff Robins</a>
														<span>Partner Startup</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-9">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">
														<!-- Change Thumbnail from Session -->
														<img style="margin: 35px;" src="<?php echo __ROOT_URL;?>/assets/images/icons/cronometer.png" alt="" width="100" height="auto" />

													</div>
												</div>
											</div>
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">10:00 - 10:30</div>
													<h2>Coffee Break</h2>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-10">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-4.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">10:30 - 12:00</div>
													<h2>Focus On Dynamics</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Robert Gates</a>
														<span>Partner Startup</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-10">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="inner-box">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">
														<!-- Change Thumbnail from Session -->
														<img style="margin: 35px;" src="<?php echo __ROOT_URL;?>/assets/images/icons/cronometer.png" alt="" width="100" height="auto" />

													</div>
												</div>
											</div>
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">12:00 - 13:30</div>
													<h2>Lunch Time!</h2>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-11">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-1.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">13:30 - 14:30</div>
													<h2>Evolution of User Experience</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Danielle Ahn</a>
														<span>Co-founder ZipCar, </span>
														<a href="#popup_1" class="ts-image-popup">Piera Mcclure</a>
														<span>Creative Director Slack</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-11">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="inner-box" data-toggle="collapse" data-target="#description-collapse-12">
										<div class="row clearfix">

											<!-- Image Column -->
											<div class="image-column col-lg-3 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="image">

														<!-- Change Thumbnail from Session -->
														<img src="<?php echo __ROOT_URL;?>/assets/images/resource/event-4.jpg" alt="" />

														<!--Overlay Box-->
														<div class="overlay-box">
															<div class="overlay-inner">
																<div class="content">
																	<h3><a href="">Change Image</a></h3>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											<!-- Info Column -->
											<div class="info-column col-lg-9 col-sm-12 col-md-12">
												<div class="inner-column">
													<div class="text">14:30 - 15:30</div>
													<h2>Building Gov as a Service</h2>
													<br>
													<div class="name">
														<a href="#popup_1" class="ts-image-popup">Piera Mcclure</a>
														<span>Creative Director Slack</span>
													</div>
													<br>
													<div class="collapse text" id="description-collapse-12">
														<div class="row clearfix">
															<div class="info-column col-lg-9 col-sm-12 col-md-12">
																<div>
																	<div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
																</div>
															</div>
															<div class="info-column col-lg-3 col-sm-12 col-md-12">
																<div class="inner-column">
																	<div class="buttons-box text-left">
																		<a href="" class="theme-btn btn-style-one">Attend</a>
																	</div>
																</div>
															</div>
														</div>
														<hr>
														<strong class="marked-text"><i class="fas fa-map-marker"></i> WHERE</strong>
														<div class="text">Hall 1, Building A</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</section>