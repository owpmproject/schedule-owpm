<div id="add-new-session" class="popup-content ts-speaker-popup mfp-hide">
    <div class="contact-form">
        <!--contact Form-->
        <form method="post" action="" id="contact-form">
            <div class="row clearfix">
            
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <input type="text" name="weekday" placeholder="Weekday" required>
                </div>
                
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <input type="email" name="month" placeholder="Month" required>
                </div>
                
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <input type="text" name="day" placeholder="Day" required>
                </div>
                
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <input type="text" name="year" placeholder="Year" required>
                </div>
                
                <div class="form-group text-center col-lg-12 col-md-12 col-sm-12">
                    <button class="theme-btn btn-style-one" type="submit" name="submit-form">Create New Day</button>
                </div>
                
            </div>
        </form>
    </div>
</div>