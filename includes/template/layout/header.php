<?php
    require(__DIR__."/../../../config.php");
?>
<!DOCTYPE html>
<html>

<!-- Mirrored from html.commonsupport.xyz/2019/miexpo/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 May 2020 23:35:38 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>

    <title>OWPM Schedule</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">

    <link rel="icon" href="/assets/img/favicon_bj.png" type="image/x-icon" />

    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/custom-animate.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/owl.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/style-2.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo __ROOT_URL;?>/assets/css/swiper.min.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>

<body>