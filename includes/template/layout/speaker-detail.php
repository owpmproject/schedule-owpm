<div id="popup_1" class="popup-content ts-speaker-popup mfp-hide">
							
    <div class="row clearfix">
    
        <!-- Image Column -->
        <div class="image-column col-lg-5 col-md-5 col-sm-12">
            <div class="inner-column">
                <div class="image">
                    <img src="<?php echo __ROOT_URL;?>/assets/images/resource/team-5.jpg" alt="">
                    <ul class="social-box">
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-behance"></span></a></li>
                        <li><a href="#"><span class="fab fa-facebook"></span></a></li>
                        <li><a href="#"><span class="fab fa-linkedin-in"></span></a></li>
                    </ul>
                </div>
                
                <!--Skills-->
                <div class="skills">
                    
                    <!--Skill Item-->
                    <div class="skill-item">
                        <div class="skill-header clearfix">
                            <div class="skill-title">Digital Marketing</div>
                            <div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="85">0</span>%</div></div>
                        </div>
                        <div class="skill-bar">
                            <div class="bar-inner"><div class="bar progress-line" data-width="85"></div></div>
                        </div>
                    </div>
                    
                    <!--Skill Item-->
                    <div class="skill-item">
                        <div class="skill-header clearfix">
                            <div class="skill-title">Strategy Planning</div>
                            <div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="70">0</span>%</div></div>
                        </div>
                        <div class="skill-bar">
                            <div class="bar-inner"><div class="bar progress-line" data-width="70"></div></div>
                        </div>
                    </div>
                    
                    <!--Skill Item-->
                    <div class="skill-item">
                        <div class="skill-header clearfix">
                            <div class="skill-title">Startup Ideas</div>
                            <div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="95">0</span>%</div></div>
                        </div>
                        <div class="skill-bar">
                            <div class="bar-inner"><div class="bar progress-line" data-width="95"></div></div>
                        </div>
                    </div>
                
                </div>
                
            </div>
        </div>
        
        <!-- Content Column -->
        <div class="content-column col-lg-7 col-md-7 col-sm-12">
            <div class="inner-column">
                <h3>Robert Gates</h3>
                <span class="speakder-designation">Speaker, Trainer</span>
                <div class="text">Dolor sit amet, consectetur adipisicing elitm sed eiusmod tempor incididunt ut labore etsu dolore magna aliquatenim minim veniam quis nostrud exercitation ulamco laboris nis aliquip consequat aute irure dolor.</div>
                <ul class="speaker-list">
                    <li><span>Profession</span>Digital Marketing</li>
                    <li><span>Experience</span>8 Years</li>
                    <li><span>Email</span><a href="mailto:robert.gates@domain.com">robert.gates@domain.com</a></li>
                    <li><span>Phone</span><a href="tell:567-123-9008">567.123.9008</a></li>
                    <li><span>Company</span>MiExpo Business</li>
                    <li><span>Website</span><a href="#">www.miexpoconf.com</a></li>
                </ul>
            </div>
        </div>
        
    </div>
    
</div>