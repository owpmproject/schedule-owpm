<?php
    require(__DIR__."/config.php");
    require(__DIR__."/includes/template/layout/header.php");
    require(__DIR__."/includes/template/layout/schedule.php");
    require(__DIR__."/includes/template/layout/speaker-detail.php");
    require(__DIR__."/includes/template/layout/scripts.php");
    require(__DIR__."/includes/template/layout/end.php");
